
$DevOpsLogRoot = "$env:public\DevOpsLogs"
$DevOps_LocalRepo = "$env:public\DevOpsConfig"
$DevOps_RepoToSync = "tiers_v1"

If (!(Test-Path $DevOpsLogRoot)) {New-Item $DevOpsLogRoot -ItemType Directory | out-null}
$LogFileBaseName = [System.IO.Path]::GetFileNameWithoutExtension($myinvocation.mycommand.definition)
$LogFilePathName = Join-Path "$DevOpsLogRoot" "$LogFileBaseName.log"
Start-Transcript "$LogFilePathName" -Append

Function Add-Shortcut {
	param(
	  [string] $shortcutFilePath,
	  [string] $targetPath,
	  [string] $workingDirectory,
	  [string] $arguments,
	  [string] $iconLocation,
	  [string] $description
	)

$global:WshShell = New-Object -com "WScript.Shell"
$lnk = $global:WshShell.CreateShortcut($shortcutFilePath)
$lnk.TargetPath = $targetPath
$lnk.WorkingDirectory = $workingDirectory
$lnk.Arguments = $arguments
if($iconLocation) {
  $lnk.IconLocation = $iconLocation
}
$lnk.Description = $description
$lnk.Save()
}

Function Add-Shortcut { 
 
[CmdletBinding()] 
param( 
    [Parameter(Mandatory=$True,  ValueFromPipelineByPropertyName=$True,Position=0)]  
    [Alias("File","Shortcut","shortcutFilePath")]  
    [string]$Path, 
 
    [Parameter(Mandatory=$True,  ValueFromPipelineByPropertyName=$True,Position=1)]  
    [Alias("Target")]  
    [string]$TargetPath, 
 
    [Parameter(ValueFromPipelineByPropertyName=$True,Position=2)]   
    [Alias("WorkingDirectory","WorkingDir")] 
    [string]$WorkDir, 

    [Parameter(ValueFromPipelineByPropertyName=$True,Position=3)]  
    [Alias("Args","Argument")]  
    [string]$Arguments, 
 
    [Parameter(ValueFromPipelineByPropertyName=$True,Position=4)]   
    [Alias("iconLocation")]  
    [string]$Icon, 
 
    [Parameter(ValueFromPipelineByPropertyName=$True,Position=5)]   
    [Alias("Desc")] 
    [string]$Description, 
 
    [Parameter(ValueFromPipelineByPropertyName=$True,Position=6)]   
    [string]$HotKey, 
 
 
    [Parameter(ValueFromPipelineByPropertyName=$True,Position=7)]   
    [int]$WindowStyle, 
 
    [Parameter(ValueFromPipelineByPropertyName=$True)]   
    [switch]$admin 
) 
 
 
Process { 
 
  If (!($Path -match "^.*(\.lnk)$")) { 
    $Path = "$Path`.lnk" 
  } 
  [System.IO.FileInfo]$Path = $Path 
  Try { 
    If (!(Test-Path $Path.DirectoryName)) { 
      md $Path.DirectoryName -ErrorAction Stop | Out-Null 
    } 
  } Catch { 
    Write-Verbose "Unable to create $($Path.DirectoryName), shortcut cannot be created" 
    Return $false 
    Break 
  } 
 
 
  # Define Shortcut Properties 
  $WshShell = New-Object -ComObject WScript.Shell 
  $Shortcut = $WshShell.CreateShortcut($Path.FullName) 
  $Shortcut.TargetPath = $TargetPath 
  $Shortcut.Arguments = $Arguments 
  $Shortcut.Description = $Description 
  $Shortcut.HotKey = $HotKey 
  $Shortcut.WorkingDirectory = $WorkDir 
  $Shortcut.WindowStyle = $WindowStyle 
  If ($Icon){ 
    $Shortcut.IconLocation = $Icon 
  } 
 
  Try { 
    # Create Shortcut 
    $Shortcut.Save() 
    # Set Shortcut to Run Elevated 
    If ($admin) {      
      $TempFileName = [IO.Path]::GetRandomFileName() 
      $TempFile = [IO.FileInfo][IO.Path]::Combine($Path.Directory, $TempFileName) 
      $Writer = New-Object System.IO.FileStream $TempFile, ([System.IO.FileMode]::Create) 
      $Reader = $Path.OpenRead() 
      While ($Reader.Position -lt $Reader.Length) { 
        $Byte = $Reader.ReadByte() 
        If ($Reader.Position -eq 22) {$Byte = 34} 
        $Writer.WriteByte($Byte) 
      } 
      $Reader.Close() 
      $Writer.Close() 
      $Path.Delete() 
      Rename-Item -Path $TempFile -NewName $Path.Name | Out-Null 
    } 
    Return $True 
  } Catch { 
    Write-Verbose "Unable to create $($Path.FullName)" 
    Write-Verbose $Error[0].Exception.Message 
    Return $False 
  } 
 
} 
} 

"`r`n`r`n[DevOpsMsg] Setting Machine PowerShell Execution Policy to RemoteSigned" | write-host -foregroundcolor yellow
Try 
  {
  Set-ExecutionPolicy -Scope LocalMachine -ExecutionPolicy RemoteSigned -Force -ErrorAction SilentlyContinue
  }
Catch {}

If (!(Test-path "$($env:ALLUSERSPROFILE)\chocolatey\bin"))
{
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1')) 
$env:path = "$($env:ALLUSERSPROFILE)\chocolatey\bin;$($env:Path)"
}
Else
{
"`r`n`r`n[DevOpsMsg] Chocolatey is already installed, skipping it..." | write-host -foregroundcolor yellow
}

#Disable Console QuickEdit mode which causes unusual pausing of interactive consoles:
New-Item 'Registry::HKCU\Console\%SystemRoot%_System32_WindowsPowerShell_v1.0_powershell.exe' -ea SilentlyContinue | New-ItemProperty -Name QuickEdit -Value 0 -PropertyType "DWord" -Force | Out-Null
New-Item 'Registry::HKCU\Console\%SystemRoot%_SysWOW64_WindowsPowerShell_v1.0_powershell.exe' -ea SilentlyContinue | New-ItemProperty -Name QuickEdit -Value 0 -PropertyType "DWord" -Force | Out-Null
Set-ItemProperty -Path 'Registry::HKCU\Console' -Name QuickEdit -Value 0 -Force | Out-Null

"`r`n`r`n[DevOpsMsg] Adding NuGet Repository to sources" | write-host -foregroundcolor yellow
choco sources add -name nuget -source https://www.nuget.org/api/v2/

"`r`n`r`n[DevOpsMsg] Current List of Sources" | write-host -foregroundcolor yellow
choco sources list

"`r`n`r`n[DevOpsMsg] Installing get-carbon Windows Configuration Modules" | write-host -foregroundcolor yellow
cinst -y carbon

$OSver = ((gwmi Win32_OperatingSystem).Version)
If ((([version]$OSver -ge [version]"6.3") -and ([version]$OSver -lt [version]"6.4")))
  {
  "`r`n`r`n[DevOpsMsg] This OS does not have a native start menu, installing Classic Shell" | write-host -foregroundcolor yellow
  cinst -y classic-shell
  }

$FirstTimeThrough = (!([bool](chocolatey list boxsstarter -localonly | where-object {$_ -ilike "BoxStarter"})))

"`r`n`r`n[DevOpsMsg] Chocolately Installed, Now Installing BoxStarter" | write-host -foregroundcolor yellow
cinst -y BoxStarter
."$env:APPDATA\BoxStarter\BoxStarterShell.ps1"
#boxstartershell

"`r`n`r`n[DevOpsMsg] Installing Git Client" | write-host -foregroundcolor yellow
#cinst -y msysgit
cinst -y git

$gitpath = 'C:\Program Files (x86)\git\cmd'
$CurrentMachinePath = [Environment]::GetEnvironmentVariable("Path", "Machine")
$CurrentProcessPath = [Environment]::GetEnvironmentVariable("Path", "Process")
if (!($CurrentMachinePath -ilike "*\git\cmd*"))
  {
  [Environment]::SetEnvironmentVariable("Path", $CurrentMachinePath + ";$gitpath", "Machine")
  }

if (!($CurrentProcessPath -ilike "*\git\cmd*"))
  {
  [Environment]::SetEnvironmentVariable("Path", $CurrentProcessPath + ";$gitpath", "Process")
  }
git config --global credential.helper wincred


"`r`n`r`n[DevOpsMsg] Cloning private config scripts repo." | write-host -foregroundcolor yellow
"`r`n`r`n            If you mess up the authentication, hit 'n' at the end of this script and issue:" | write-host -foregroundcolor yellow
"`r`n`r`n              $DevOps_LocalRepo\clonecommand.cmd" | write-host -foregroundcolor yellow
"`r`n`r`n              DONT FORGET TO REBOOT AFTERWARD!!!" | write-host -foregroundcolor yellow

If (Test-Path $DevOps_LocalRepo) {remove-item $DevOps_LocalRepo -recurse -force -ea silentlycontinue}
md $DevOps_LocalRepo -Force
cd $DevOps_LocalRepo

"git clone --depth 1 https://bitbucket.org/qpdo/$DevOps_RepoToSync & pause" | Out-File "$DevOps_LocalRepo\CLONEconfigscripts.cmd" -Encoding ASCII
"CD $DevOps_RepoToSync & git pull & pause" | Out-File "$DevOps_LocalRepo\UPDATEconfigscripts.cmd" -Encoding ASCII

[Environment]::SetEnvironmentVariable("DEVOPSCONFIG", "$DevOps_LocalRepo\$DevOps_RepoToSync", "Machine")
[Environment]::SetEnvironmentVariable("DEVOPSCONFIG", "$DevOps_LocalRepo\$DevOps_RepoToSync", "Process")

"`r`n`r`n[DevOpsMsg] Setting Up Shortcuts and Environment Variables" | write-host -foregroundcolor yellow
Add-Shortcut "$env:public\Desktop\DevOps Configuration Prompt.lnk" "$env:windir\system32\windowspowershell\v1.0\powershell.exe" -Arguments "-noexit -command `"cd $env:devopsconfig`"" -admin

Add-Shortcut -shortcutFilePath "$env:public\desktop\BoxStarter Log.lnk" -targetPath "$env:userprofile\AppData\Local\Boxstarter\boxstarter.log" -workingDirectory "$env:userprofile\AppData\Local\Boxstarter" -Description "BoxStarter Log"

Add-Shortcut -shortcutFilePath "$env:public\desktop\Chocolatey Logs Folder.lnk" -targetPath "$env:userprofile\appdata\local\Temp\chocolatey" -workingDirectory "$env:userprofile\appdata\local\Temp\chocolatey" -Description "Chocolatey Logs Folder"

$FailCount = 0
Do 
  {
  If ($FailCount -gt 0) {"Opps, Try again :) " | write-host -foregroundcolor yellow}
  git clone --depth 1 https://bitbucket.org/qpdo/$DevOps_RepoToSync
  ++$FailCount
  } Until (Test-Path "$DevOps_LocalRepo\$DevOps_RepoToSync")

stop-transcript

"`r`n`r`n[DevOpsMsg] Continuing remaining tasks in BoxStarter" | write-host -foregroundcolor yellow
Install-BoxstarterPackage -PackageName https://bitbucket.org/qpdo/bootstrap/raw/master/bootstrap-layer.zboxstarter
